<?php
/**
 * docker helper functions
 *
 * @author: tuanha
 * @last-mod: 05-Apr-2021
 */
if (! function_exists('docker_secret')) {
    /**
     * Load secret data from file given by $envName . '_FILE'
     * use in case of being containerized with Docker secret
     *
     * @param  string $envName
     * @param  $default
     */
    function docker_secret(string $envName, $default = null)
    {
        $envName_FILE = env($envName . '_FILE');
        if ($envName_FILE && file_exists($envName_FILE)) {
            return trim(file_get_contents($envName_FILE));
        } else {
            return env($envName, $default);
        }
    }
}
