<?php
/**
 * ProcessSSLChecking
 *
 * @author: tuanha
 * @date: 20-Apr-2020
 */
namespace App\Jobs;

use App\Exports\ExcelExport;
use App\Mail\CheckSSLResult;
use App\Traits\InspectDomain;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Bkstar123\LogEnhancer\Facades\DebugLog;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessSSLChecking implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, InspectDomain;

    /**
     * @var array
     */
    protected $domains;

    /**
     * @var string
     */
    protected $email;

    /**
     * The number of seconds the job can run before timing out
     * must be on several seconds less than the queue connection's retry_after defined in the config/queue.php
     *
     * @var int
     */
    public $timeout = 1190;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($domains, $email)
    {
        $this->domains = $domains;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = [];
        foreach ($this->domains as $domain) {
            DebugLog::log('info', "Proceeding SSL check for {domain}", ['domain' => trim($domain)]);
            $dnsRecords = $this->getDNSRecords($domain);
            $cert = $this->getSSLCertificate($domain);
            if (!is_null($cert)) {
                array_push($data, [
                    'URL' => trim($domain),
                    'Issuer' => $cert->getIssuer(),
                    'Valid from' => $cert->validFromDate(),
                    'Expired at' => $cert->expirationDate(),
                    'CN' => $cert->getDomain(),
                    'Fingerprint' => $cert->getFingerprint(),
                    'Remaining days' => $cert->daysUntilExpirationDate(),
                    'A' => implode(',', $dnsRecords['A']),
                    'CNAME' => implode(',', $dnsRecords['CNAME']),
                    'SAN' => implode(',', $cert->getAdditionalDomains()),
                ]);
            } else {
                array_push($data, [
                    'URL' => $domain,
                    'Issuer' => '',
                    'Valid from' => '',
                    'Expired at' => '',
                    'CN' => '',
                    'Fingerprint' => '',
                    'Remaining days' => '',
                    'A' => implode(',', $dnsRecords['A']),
                    'CNAME' => implode(',', $dnsRecords['CNAME']),
                    'SAN' => '',
                ]);
            }
        }
        $headings = [
            'URL',
            'Issuer',
            'Valid from',
            'Expired at',
            'CN',
            'Fingerprint',
            'Remaining days',
            'A',
            'CNAME',
            'SAN'
        ];
        Mail::to($this->email)
            ->send(new CheckSSLResult(Excel::raw(new ExcelExport($data, $headings), 'Xlsx'), $this->domains));
    }
}
