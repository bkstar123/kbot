<?php

namespace App\Jobs;

use App\Exports\ExcelExport;
use App\Mail\CheckDNSResult;
use App\Traits\InspectDomain;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Bkstar123\LogEnhancer\Facades\DebugLog;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessDNSChecking implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, InspectDomain;

    /**
     * @var array
     */
    protected $domains;

    /**
     * @var string
     */
    protected $email;

    /**
     * The number of seconds the job can run before timing out
     * must be on several seconds less than the queue connection's retry_after defined in the config/queue.php
     *
     * @var int
     */
    public $timeout = 1190;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($domains, $email)
    {
        $this->domains = $domains;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = [];
        foreach ($this->domains as $domain) {
            DebugLog::log('info', "Proceeding DNS check for {domain}", ['domain' => trim($domain)]);
            $dnsRecords = $this->getDNSRecords($domain);
            array_push($data, [
                trim($domain), 
                implode(',', $dnsRecords['A']), 
                implode(',', $dnsRecords['CNAME'])
            ]);
        }
        $headings = [
            'URL',
            'A',
            'CNAME'
        ];
        Mail::to($this->email)
            ->send(new CheckDNSResult(Excel::raw(new ExcelExport($data, $headings), 'Xlsx'), $this->domains));
    }
}
