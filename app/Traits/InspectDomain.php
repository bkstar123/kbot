<?php
/**
 * InspectDomain
 *
 * @author: tuanha
 * @date: 20-Apr-2020
 */
namespace App\Traits;

use Exception;
use Spatie\SslCertificate\SslCertificate;

trait InspectDomain
{
    /**
     * @param string $domain
     * @return array
     */
    protected function getDNSRecords($domain)
    {
        $domain = idn_to_ascii(trim($domain), IDNA_DEFAULT, INTL_IDNA_VARIANT_UTS46);
        $IPs = [];
        $Aliases = [];
        try {
            $a_records = dns_get_record($domain, DNS_A);
        } catch (Exception $e) {
            $a_records = [];
        }
        try {
            $cname_records = dns_get_record($domain, DNS_CNAME);
        } catch (Exception $e) {
            $cname_records = [];
        }
        if (!empty($a_records)) {
            foreach ($a_records as $record) {
                array_push($IPs, $record['ip']);
            }
        }
        if (!empty($cname_records)) {
            foreach ($cname_records as $record) {
                array_push($Aliases, $record['target']);
            }
        }
        return [
            'A' => $IPs,
            'CNAME' => $Aliases
        ];
    }

    /**
     * @param string $domain
     * @return \Spatie\SslCertificate\SslCertificate | null
     */
    protected function getSSLCertificate($domain)
    {
        $domain = idn_to_ascii(trim($domain), IDNA_DEFAULT, INTL_IDNA_VARIANT_UTS46);
        try {
            $cert = SslCertificate::createForHostName($domain);
            return $cert;
        } catch (Exception $e) {
            return null;
        }
    }
}
