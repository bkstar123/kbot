<?php
/**
 * KbotCOntroller
 *
 * @author: tuanha
 * @date: 26-Apr-2020
 */
namespace App\Http\Controllers;

use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;
use App\Conversations\CheckDNSConversation;
use App\Conversations\CheckSSLConversation;
use App\Conversations\DecodeCsrConversation;
use App\Conversations\DecodeCertificateConversation;
use App\Conversations\ExtractRootDomainsConversation;

class KbotController extends Controller
{
    /**
     * @param \BotMan\BotMan\BotMan $bot
     */
    public function startCheckSSLConversation(BotMan $bot)
    {
        $bot->startConversation(new CheckSSLConversation);
    }

    /**
     * @param \BotMan\BotMan\BotMan $bot
     */
    public function startCheckDNSConversation(BotMan $bot)
    {
        $bot->startConversation(new CheckDNSConversation);
    }

    /**
     * @param \BotMan\BotMan\BotMan $bot
     */
    public function startDecodeCertificateConversation(BotMan $bot)
    {
        $bot->startConversation(new DecodeCertificateConversation);
    }

    /**
     * @param \BotMan\BotMan\BotMan $bot
     */
    public function startExtractRootDomainsConversation(BotMan $bot)
    {
        $bot->startConversation(new ExtractRootDomainsConversation);
    }

    /**
     * @param \BotMan\BotMan\BotMan $bot
     */
    public function startDecodeCsrConversation(BotMan $bot)
    {
        $bot->startConversation(new DecodeCsrConversation);
    }
}
