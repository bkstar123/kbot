<?php

namespace App\Http\Controllers;

use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;

class GenericCommandController extends Controller
{
    /**
     * Reply to user "Hi|Hello" command
     *
     * @param \BotMan\BotMan\BotMan
     * @return mixed
     */
    public function replyToHi(BotMan $bot)
    {
        $bot->reply('Hi there');
        $bot->reply('Please type "services" to see what kinds of services I can offer, or type "help" for any further detailed description');
    }

    /**
     * Reply to user "serivces" command
     *
     * @param \BotMan\BotMan\BotMan
     * @return mixed
     */
    public function replyToServices(BotMan $bot)
    {
        $question = Question::create('What do you want me to do?')
                        ->addButtons([
                            Button::create('Check SSL')->value('check ssl'),
                            Button::create('Check DNS')->value('check dns'),
                            Button::create('Decode Cert')->value('decode cert'),
                            Button::create('Decode CSR')->value('decode csr'),
                            Button::create('Extract Root Domains')->value('extract root domains'),
                    ]);
        $bot->ask($question, function (Answer $ans, $con) {
            if (!$ans->isInteractiveMessageReply()) {
                if (!in_array($ans->getText(), ['check ssl', 'check dns', 'decode cert', 'decode csr', 'extract root domains'])) {
                    $con->repeat("Wrong command has been given, you can just click on the above options, or type one of the following commands: check ssl, check dns, decode cert, extract root domains");
                }
            }
        });
    }

    /**
     * Reply to user "Stop" command
     *
     * @param \BotMan\BotMan\BotMan
     * @return mixed
     */
    public function replyToStop(BotMan $bot)
    {
        $bot->reply('OK then, please let me know if you need anything else');
        $bot->reply('Just type "services" to see what kinds of services I can offer, or type "help" for any further detailed description');
    }

    /**
     * Reply to user "Help" command
     *
     * @param \BotMan\BotMan\BotMan
     * @return mixed
     */
    public function replyToHelp(BotMan $bot)
    {
        $bot->reply('For the time being, I can understand the following commands (case-insensitive)');
        $bot->reply('check ssl, check dns, decode cert, decode csr, extract root domains');
        $bot->reply('Further explanation...');
        $explaination = '"check ssl" to check TLS/SSL for the list of domains separated by commas.';
        $explaination .= ' "check dns" to check A/CNAME records for the list of domains separated by commas.';
        $bot->reply($explaination);
        $explaination = '"decode cert" to reveal basic public information of a certificate, "decode csr" to reveal the data of a certificate signing request ';
        $bot->reply($explaination);
        $bot->reply('"extract root domains" to get the list of unique root/apex domains from the given list of hostnames separated by commas');
        $bot->reply('An Apex domain, is a root domain that does not contain a subdomain. They are also known as base, bare, naked, root apex, or zone apex domains.');
        $bot->reply('For examples: example.com is a apex domain because it does not have a subdomain. www.example.com is not an apex domain because it contains the subdomain part www.');
        $bot->reply('To abort any of these commands, just type "Stop"');
        $bot->reply('To quickly check what email address you have given to me without having to scroll up, just ask "tell me my email"');
    }

    /**
     * Reply to user "Tell me my email" command
     *
     * @param \BotMan\BotMan\BotMan
     * @return mixed
     */
    public function replyToTellMeMyEmail(BotMan $bot)
    {
        if (!empty($bot->userStorage()->get('email'))) {
            $bot->reply("You have just given it to me as " . $bot->userStorage()->get('email'));
        } else {
            $bot->reply('You have not told me yet, or you may have already given but then reloaded the page');
            $bot->ask(
                'What is your email address? (you can type "stop" if you do not want to give it to me)',
                function ($ans, $conversation) {
                    $email = $ans->getText();
                    $validator = Validator::make([
                        'email' => $email
                    ], [
                        'email' => 'required|email',
                    ]);
                    if ($validator->fails()) {
                        $conversation->say('This is not a valid email address');
                        return $conversation->repeat('Please type your email again, or enter "stop" to leave the conversation');
                    } else {
                        $conversation->getBot()->userStorage()->save([
                            'email' => $email
                        ]);
                        $conversation->say('OK, I remember. Your email is ' . $email);
                        $conversation->say('Do not reload page until you finish');
                    }
                }
            );
        }
    }
}
