<?php
/**
 * CheckSSLConversation
 *
 * @author: tuanha
 * @date: 26-Apr-2020
 */
namespace App\Conversations;

use App\Traits\InspectDomain;
use App\Jobs\ProcessSSLChecking;
use Illuminate\Support\Facades\Validator;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Conversations\Conversation;

class CheckSSLConversation extends Conversation
{
    use InspectDomain;

    /**
     * Ask for the list of domains
     *
     * @return mixed
     */
    protected function askDomains()
    {
        $this->ask('Sure, please give me the list of domains which are separated by commas', function (Answer $ans) {
            $list = $ans->getText();
            $domains = explode(',', $list);
            if (count($domains) > 1) {
                $email = $this->getBot()->userStorage()->get('email');
                if (empty($email)) {
                    $this->askEmail($domains);
                } else {
                    $this->say("OK, I will send the results to " . $email);
                    $this->say("For the domains: " . implode(',', $domains));
                    ProcessSSLChecking::dispatch($domains, $email);
                }
            } else {
                $this->checkSSLOneDomain($domains[0]);
            }
        });
    }

    /**
     * Ask for user email address
     *
     * @return mixed
     */
    protected function askEmail($domains)
    {
        $this->ask(
            'You asked for more than 01 domain, please give me your email address, I will then e-mail the result to you',
            function (Answer $ans) use ($domains) {
                $email = $ans->getText();
                $validator = Validator::make([
                    'email' => $email
                ], [
                    'email' => 'required|email',
                ]);
                if ($validator->fails()) {
                    $this->say('This is not a valid email address');
                    return $this->repeat('Please type your email again, or enter "stop" to leave the conversation. Hpwever, you will not get the result)');
                } else {
                    $this->getBot()->userStorage()->save([
                        'email' => $email
                    ]);
                }
                $this->say("OK, I will send the result to " . $email);
                $this->say("For the following domains: " . implode(',', $domains));
                ProcessSSLChecking::dispatch($domains, $this->getBot()->userStorage()->get('email'));
            }
        );
    }

    /**
     * checkSSLOneDomain
     *
     * @param string $domain
     * @return mixed
     */
    protected function checkSSLOneDomain($domain)
    {
        $dnsRecords = $this->getDNSRecords($domain);
        $cert = $this->getSSLCertificate($domain);
        if (!is_null($cert)) {
            $msgFormat = "%s => certificate is issued by %s, valid from %s, expired on %s, fingerprint: %s, %s days left, A: %s, CNAME: %s, SAN: %s";
            $message = sprintf(
                $msgFormat,
                $domain,
                $cert->getIssuer(),
                $cert->validFromDate(),
                $cert->expirationDate(),
                $cert->getFingerprint(),
                $cert->daysUntilExpirationDate(),
                json_encode($dnsRecords['A']),
                json_encode($dnsRecords['CNAME']),
                implode(',', $cert->getAdditionalDomains())
            );
            $this->say($message);
        } else {
            $msgFormat = "%s => A: %s, CNAME: %s";
            $message = sprintf(
                $msgFormat,
                $domain,
                json_encode($dnsRecords['A']),
                json_encode($dnsRecords['CNAME'])
            );
            $this->say($message);
        }
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->askDomains();
    }
}
