<?php

namespace App\Conversations;

use Exception;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Conversations\Conversation;

class DecodeCsrConversation extends Conversation
{
    /**
     * Ask for the CSR
     *
     * @return mixed
     */
    protected function askCSR()
    {
        $this->ask('OK, please paste the content of your CSR here', function (Answer $ans) {
            $csr = $ans->getText();
            $csr = preg_replace('/\s(?!CERTIFICATE|REQUEST)/', "\n", $csr);
            try {
                $data = openssl_csr_get_subject($csr);
                $msgFormat = "Common Name: %s.
                    State Or Province:  %s.
                    Country: %s.
                    Locality: %s.
                    Organization: %s.
                    Organization Unit: %s.
                    Email: %s";
                $msg = sprintf(
                    $msgFormat,
                    array_key_exists('CN', $data) ? $data['CN'] : '',
                    array_key_exists('ST', $data) ? $data['ST'] : '',
                    array_key_exists('C', $data) ? $data['C'] : '',
                    array_key_exists('L', $data) ? $data['L'] : '',
                    array_key_exists('O', $data) ? $data['O'] : '',
                    array_key_exists('OU', $data) ? $data['OU'] : '',
                    array_key_exists('emailAddress', $data) ? $data['emailAddress'] : ''
                );
                $this->say(($msg));
            } catch (Exception $e) {
                return $this->repeat('I cannot parse the given CSR as it seems to be invalid. Please re-paste the CSR content, or type "stop" to abort the command');
            }
        });
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->askCSR();
    }
}
