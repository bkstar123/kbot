<?php

namespace App\Conversations;

use Exception;
use Carbon\Carbon;
use Spatie\SslCertificate\SslCertificate;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Conversations\Conversation;

class DecodeCertificateConversation extends Conversation
{
    /**
     * Ask for the certificate in .crt, cer
     *
     * @return mixed
     */
    protected function askCertificate()
    {
        $this->ask('OK, please paste the content of your certificate here', function (Answer $ans) {
            $certificate = $ans->getText();
            $certificate = preg_replace('/\s(?!CERTIFICATE)/', "\n", $certificate);
            try {
                $ssl = SslCertificate::createFromString($certificate);
                $msgFormat = "Common Name: %s.
                    Organization: %s.
                    Organization Unit: %s.
                    Locality: %s.
                    State: %s.
                    Country: %s.
                    Valid from: %s. 
                    Valid until: %s. 
                    Issuer: %s.
                    Fingerprint: %s.
                    SAN: %s";
                $msg = sprintf(
                    $msgFormat,
                    $ssl->getDomain(),
                    $ssl->getOrganization(),
                    array_key_exists('OU', $ssl->getRawCertificateFields()['subject']) ? $ssl->getRawCertificateFields()['subject']['OU'] : '',
                    array_key_exists('L', $ssl->getRawCertificateFields()['subject']) ? $ssl->getRawCertificateFields()['subject']['L'] : '',
                    array_key_exists('ST', $ssl->getRawCertificateFields()['subject']) ? $ssl->getRawCertificateFields()['subject']['ST'] : '',
                    array_key_exists('C', $ssl->getRawCertificateFields()['subject']) ? $ssl->getRawCertificateFields()['subject']['C'] : '',
                    $ssl->validFromDate(),
                    $ssl->expirationDate(),
                    $ssl->getIssuer(),
                    $ssl->getFingerprint(),
                    implode(',', $ssl->getAdditionalDomains())
                );
                $this->say(($msg));
            } catch (Exception $e) {
                return $this->repeat('I cannot parse the given certificate as it seems to be invalid. Please re-paste the certificate content, or type "stop" to abort the command');
            }
        });
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->askCertificate();
    }
}
