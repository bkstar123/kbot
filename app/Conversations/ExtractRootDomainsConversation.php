<?php

namespace App\Conversations;

use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Conversations\Conversation;

class ExtractRootDomainsConversation extends Conversation
{
    /**
     * Ask for the list of domains
     *
     * @return mixed
     */
    protected function askDomains()
    {
        $this->ask('Sure, please give me the list of domains which are separated by commas', function (Answer $ans) {
            $list = $ans->getText();
            $domains = explode(',', $list);
            $TLDs = file_get_contents(asset('/sources/tlds.txt'));
            $TLDs = explode(',', $TLDs);
            $apexZones = [];
            foreach ($domains as $index => $domain) {
                $domainParts = explode('.', trim($domain));
                $i = count($domainParts) - 1;
                $apexZone = $domainParts[$i];
                while ($i >= 0 && in_array($apexZone, $TLDs)) {
                    --$i;
                    $apexZone = $domainParts[$i].'.'.$apexZone;
                }
                $apexZones[] = $apexZone;
            }
            $this->say('Kindly find the list of unique root/apex domains as follows:');
            $apexZones = array_map(function ($zone) {
                return strtolower($zone);
            }, $apexZones);
            $this->say(implode(',', array_merge([], array_unique($apexZones))));
        });
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->askDomains();
    }
}
