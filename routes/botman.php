<?php
/**
 * @author: tuanha
 */
use App\Http\Controllers\KbotController;
use App\Http\Controllers\GenericCommandController;

$botman = resolve('botman');

$botman->hears('(^Hi|Hello$)', GenericCommandController::class . '@replyToHi')->skipsConversation();

$botman->hears('services', GenericCommandController::class . '@replyToServices')->skipsConversation();

$botman->hears('stop', GenericCommandController::class . '@replyToStop')->stopsConversation();

$botman->hears('help', GenericCommandController::class . '@replyToHelp')->skipsConversation();

$botman->hears('tell me my email', GenericCommandController::class . '@replyToTellMeMyEmail')->skipsConversation();

$botman->hears('check ssl', KbotController::class . '@startCheckSSLConversation')->skipsConversation();

$botman->hears('check dns', KbotController::class . '@startCheckDNSConversation')->skipsConversation();

$botman->hears('decode cert', KbotController::class . '@startDecodeCertificateConversation')->skipsConversation();

$botman->hears('decode csr', KbotController::class . '@startDecodeCsrConversation')->skipsConversation();

$botman->hears('extract root domains', KbotController::class . '@startExtractRootDomainsConversation')->skipsConversation();

$botman->fallback(function ($bot) {
    $bot->reply('Sorry, I cannot understand your command. Please type "services" for kinds of services I can offer, or "help" for any further details');
});
