<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }} - The SSL Assistant</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet" type="text/css">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <style>
        body {
            font-family: "Varela Round", sans-serif;
            margin: 0;
            padding: 0;
            background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(140,99,184,0.1516981792717087) 0%, rgba(0,212,255,1) 100%);
        }
        .container {
            display: flex;
            height: 100vh;
            align-items: center;
            justify-content: center;
        }
        .content {
            text-align: center;
        }
        .logo {
            margin-right: 40px;
            margin-bottom: 20px;
        }
        .links a {
            font-size: 1.25rem;
            text-decoration: none;
            color: white;
            margin: 10px;
        }
        @media all and (max-width: 500px) {
            .links {
                display: flex;
                flex-direction: column;
            }
        }
    </style>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<div class="container">
    <div class="content" id="app">
        <div class="logo">
            <svg width="300" height="300" enable-background="new 0 0 502.857 502.857" version="1.1" viewBox="0 0 502.86 502.86" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><g fill="#57C927"><path d="m115.43 155.43v217.66c0 17 10.208 30.336 27.704 30.336h22.84c-0.784 0-2.544 5.768-2.544 8.6v61.648c0 16.112 15.448 29.176 32 29.176 16.56 0 32-13.064 32-29.176v-61.648c0-2.832-3.088-8.6-3.848-8.6h55.712c-0.76 0-3.864 5.768-3.864 8.6v61.648c0 16.112 15.416 29.176 31.968 29.176 16.592 0 32.032-13.064 32.032-29.176v-61.648c0-2.832-1.752-8.6-2.536-8.6h22.872c17.496 0 27.664-13.336 27.664-30.336v-217.66h-273.83 1.832z"/><path d="m59.428 158.98c-16.568 0-32 13.072-32 29.176v124.92c0 16.112 15.432 29.176 32 29.176 16.56 0 32-13.064 32-29.176v-124.91c0-16.112-15.44-29.184-32-29.184z"/><path d="m320.3 42.057 5.584-8.192 5.592-8.096 12.456-18.2c1.56-2.256 0.912-5.264-1.384-6.744-2.272-1.512-5.416-0.88-6.904 1.36l-19.016 27.704-5.72 8.344c-18.072-6.832-38.208-10.64-59.48-10.64-21.224 0-41.4 3.816-59.472 10.64l-5.688-8.336-5.624-8.184-13.36-19.512c-1.544-2.248-4.648-2.84-6.952-1.36-2.28 1.488-2.912 4.496-1.392 6.744l12.448 18.208 5.592 8.104 5.616 8.168c-42.432 19.24-71.144 57.368-71.144 97.368h279.96c0-40-28.704-78.128-71.112-97.376zm-128.86 58.536c-8.312 0-15.008-6.536-15.008-14.608s6.696-14.576 15.008-14.576c8.288 0 15 6.504 15 14.576s-6.704 14.608-15 14.608zm120 0c-8.304 0-15.016-6.536-15.016-14.608s6.712-14.576 15.016-14.576c8.288 0 15 6.504 15 14.576s-6.712 14.608-15 14.608z"/></g><path d="m60.852 224.19c-12.472 0-25.424-11.768-33.424-30.432v119.32c0 16.112 15.432 29.176 32 29.176 16.56 0 32-13.064 32-29.176v-113.1c-8 14.992-19.568 24.208-30.576 24.208z" fill="#1CB71C"/><path d="m443.43 158.98c-16.568 0-32 13.072-32 29.176v124.92c0 16.112 15.432 29.176 32 29.176 16.56 0 32-13.064 32-29.176v-124.91c0-16.112-15.44-29.184-32-29.184z" fill="#57C927"/><g fill="#1CB71C"><path d="m444.85 224.19c-12.472 0-25.424-11.768-33.424-30.432v119.32c0 16.112 15.432 29.176 32 29.176 16.56 0 32-13.064 32-29.176v-113.1c-8 14.992-19.568 24.208-30.576 24.208z"/><path d="m251.43 179.34c-63.28 0-120-7.32-136-17.712v211.47c0 17 10.208 30.336 27.704 30.336h22.84c-0.784 0-2.544 5.768-2.544 8.6v61.648c0 16.112 15.448 29.176 32 29.176 16.56 0 32-13.064 32-29.176v-61.648c0-2.832-3.088-8.6-3.848-8.6h55.712c-0.76 0-3.864 5.768-3.864 8.6v61.648c0 16.112 15.416 29.176 31.968 29.176 16.592 0 32.032-13.064 32.032-29.176v-61.648c0-2.832-1.752-8.6-2.536-8.6h22.872c17.496 0 27.664-13.336 27.664-30.336v-211.48c-16 10.392-72.712 17.72-136 17.72z"/><path d="m326.44 85.977c0 8.072-6.712 14.608-15 14.608-8.304 0-15.016-6.536-15.016-14.608 0-4.376 2.008-8.24 5.136-10.912-15.816-2.64-32.64-4.088-50.128-4.088s-34.304 1.448-50.128 4.088c3.136 2.664 5.144 6.536 5.144 10.912 0 8.072-6.712 14.608-15 14.608-8.312 0-15.008-6.536-15.008-14.608 0-2.064 0.456-4.024 1.248-5.808-23.984 6.304-44.592 15.504-60.144 26.808-3.92 10.296-6.088 24.456-6.088 32.456h279.96c0-8-2.168-22.152-6.08-32.44-15.544-11.32-36.16-20.536-60.128-26.84 0.784 1.784 1.232 3.768 1.232 5.824z"/></g><path d="m251.43 262.82c-53.896 0-104-10.632-136-28.056v138.34c0 17 10.208 30.336 27.704 30.336h22.84c-0.784 0-2.544 5.768-2.544 8.6v61.648c0 16.112 15.448 29.176 32 29.176 16.56 0 32-13.064 32-29.176v-61.648c0-2.832-3.088-8.6-3.848-8.6h55.712c-0.76 0-3.864 5.768-3.864 8.6v61.648c0 16.112 15.416 29.176 31.968 29.176 16.592 0 32.032-13.064 32.032-29.176v-61.648c0-2.832-1.752-8.6-2.536-8.6h22.872c17.496 0 27.664-13.336 27.664-30.336v-138.34c-32 17.432-82.104 28.056-136 28.056z" fill="#049E42"/></svg>       
        </div>
        <div class="links">
            <button class="btn btn-block btn-info" onclick="botmanChatWidget.say('Hi');">Say Hi to KBot</button>
        </div>
    </div>
</div>
<script>
    var botmanWidget = {
        mainColor: 'linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(125,212,122,1) 0%, rgba(0,212,255,1) 100%)',
        timeFormat: 'm/d/yy HH:MM',
        title: 'KBOT',
        aboutText: 'KBOT - The SSL Assistant',
        aboutLink: '/',
        bubbleAvatarUrl: @json(config('app.url') . '/img/kbot-avatar.jpeg'),
        desktopWidth: '450px',
        introMessage: 'My name is KBOT, I am your SSL assistant today. Please say "Hi" to start chatting with me'
    };
</script>
<script src='https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/js/widget.js'></script>
<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>