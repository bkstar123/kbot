# Abstract

You can deploy this application using Docker according to the instruction in https://github.com/bkstar123/containerized-kbot   

KBOT is a chatbot built with Botman framework to provide the following capabilities:  
- Check TLS/SSL certificate information for a list of domains, email the result in csv format (it can then be imported to Excel)  
- Check DNS A/CNAME records for a list of domains, email the result in csv format (it can then be imported to Excel)  
- Decode TLS/SSL certificate data  
- Decode Certificate Signing Request (CSR)  
- Extract unique root/apex zone from a list of domains  

The application UI is as follows:  
<img src="https://github.com/bkstar123/containerized-kbot/blob/master/pictures/applicaiton_ui.png?raw=true" alt="Application UI" style="width:100%;"/>  